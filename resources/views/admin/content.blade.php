@extends('admin')

@section('content')

<div class="container">
    <div class="row justify-content-center">
      @if ($errors->any())
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-danger" role="alert">
          @foreach ($errors->all() as $error)
              <p style="margin-bottom: 0rem;"><i class="fas fa-times"></i> {{ $error }}</p>
          @endforeach
        </div>
      </div>
      @endif
      @if (session('status'))
      <div class="col-md-12 text-center" style="padding: 10px;">
        <div class="alert alert-success" role="alert">
            <i class="fas fa-check"></i> {{ session('status') }}
        </div>
      </div>
      @endif
      
      <div class="col-md-12 text-center" style="padding: 10px;">
        <h3>{{ $content->titre }}</h3>
      </div>

      <div class="d-flex justify-content-center">
        
        <form action="{{ route('admin-save',['page'=>$content->slug])}}" method="POST">
            @csrf
            <div class="form-group">
                <label>Texte</label>
                <textarea class="form-control description" name="description" maxlength="255" rows="4" required>{{ $content->description }}</textarea>
            </div>
            <div class="form-group">
                <label>Photo actuelle : </label>
                <img src="{{asset ('images/'.$content->photo)}}" style="width: 20%;"></img> 
            </div>
            <div class="form-group">
                <div class="custom-file">
                    <input accept='image/*' name="photo" type="file" class="custom-file-input" lang="fr">
                    <label id="photo" class="custom-file-label"></label>
                </div>
            </div>
            <div class="col text-center">
              <button type="submit" class="btn btn-success"><i class="fas fa-check"></i> Confirmer</button>
            </div>
        </form>
          
      </div>


    </div>
    <script>CKEDITOR.replaceClass="description";</script>
@endsection
