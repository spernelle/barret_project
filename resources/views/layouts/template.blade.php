<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Votre solution transport dans le Jura - Dept 39">
  <meta name="author" content="JB Sprint Services">
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />
  <title>JB Sprint Services - Votre solution transport dans le Jura - Dept 39</title>
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/all.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/simple-line-icons.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="{{ asset('assets/css/landing-page.min.css') }}" rel="stylesheet">
  <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
</head>

<body>

  <nav class="navbar navbar-expand-md navbar-light bg-light shadow-sm fixed-top">
    <div class="container">
      <a class="navbar-brand" href="{{ route('accueil') }}">
        <img src="{{ asset('logo.png') }}" height="60" alt="">
      </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link text-dark">Votre solution transport dans le Jura</a>
      </li>
      </ul>
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0"></ul>
      <ul class="navbar-nav navbar-right">
        <li class="nav-item">
          <a class="nav-link" id="transport" href="#"><i class="fas fa-truck"></i> Transport express</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="photos" href="#"><i class="fas fa-camera"></i> Photos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="contact" href="#"><i class="fas fa-phone-alt"></i> Nous contacter</a>
        </li>
    </ul>
    </div>
  </div>
  </nav>

    @yield("content")

    <style>
    a {
        color: #000;
        text-decoration: none;
        background-color: transparent;
    }

    a:hover {
        color: #e74c3c;
        text-decoration: none;
        background-color: transparent;
    }
    </style>

    <script>
    $('.nav-link').on('click', function(){
        $('#navbarTogglerDemo02').collapse('hide');
    });
    $('#transport').on('click', function(){
      $('html,body').animate({ scrollTop: $("#transport_div").offset().top }, 'slow');
    });
    $('#photos').on('click', function(){
      $('html,body').animate({ scrollTop: $("#photos_div").offset().top }, 'slow');
    });
    $('#contact').on('click', function(){
      $('html,body').animate({ scrollTop: $("#contact_div").offset().top }, 'slow');
    });
    </script>

  <footer class="footer bg-light">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
          <p class="text-muted small mb-4 mb-lg-0">© JB Sprint Services 2020. Tous droits réservés.</p>
        </div>
      </div>
    </div>
  </footer>

</body>

</html>
