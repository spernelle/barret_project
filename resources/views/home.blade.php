@extends('layouts.template')

@section('content')
<section id="transport_div" class="showcase bg-light">
    <div class="container-fluid p-0">
      <div class="row no-gutters">
        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('{{ asset('images') }}/{{ $accueil->photo }}');"></div>
        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
          <p class="lead mb-0">{!! $accueil->description !!}</p>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-lg-6 text-white showcase-img" style="background-image: url('{{ asset('images') }}/{{ $transport->photo }}');"></div>
        <div class="col-lg-6 my-auto showcase-text">
          <p class="lead mb-0">{!! $transport->description !!}</p>
        </div>
      </div>
    </div>
  </section>

<section class="testimonials text-center bg-white">
  <div class="container">
    <h2 class="mb-5">CHARGE MAXI / VOLUME</h2>
    <div class="row">
      @foreach($flotte as $f)
      <div class="col-lg-4">
        <div class="testimonial-item mx-auto mb-5 mb-lg-0">
          <img class="img-fluid rounded-circle mb-3" src="{{ asset('images/logo-flotte.png') }}" alt="">
          <h5>{{ $f->vehicule }}</h5>
          <p class="font-weight-light mb-0">Charge max : {{ $f->charge }}</p>
          <p class="font-weight-light mb-0">Hauteur max : {{ $f->hauteur }}</p>
          <p class="font-weight-light mb-5">Nombre(s) de palette(s) 80x120 : {{ $f->nombre }}</p>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>

<section id="photos_div" class="features-icons bg-light text-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
           <h2 class="mb-5">PHOTOS</h2>
       </div>

       @foreach($photo as $p)
        <div class="modal fade" id="g{{ $p->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h6 class="modal-title" id="g{{ $p->id }}">{{ $p->titre }}</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <img class="img-fluid img-thumbnail" src="{{ asset('photos') }}/{{ $p->photo }}" alt="{{ $p->titre }}"></img>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-4 col-6">
        <a href="#" data-toggle="modal" data-target="#g{{ $p->id }}" class="d-block mb-4 h-100">
          <img class="img-fluid img-thumbnail" src="{{ asset('photos') }}/{{ $p->photo }}" alt="{{ $p->titre }}">
          <p style="padding: 10px">{{ $p->titre }}</p>
        </a>
      </div>
      @endforeach
  </div>
</div>
</section>

<section id="contact_div" class="features-icons bg-white text-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
           <h2 class="mb-5">NOUS CONTACTER</h2>
       </div>
        <div class="col-lg-6">
          <p>19 rue du Bois, 39410 Aumur, France</p>
          <iframe width="100%" height="230" src="https://maps.google.com/maps?width=700&amp;height=440&amp;hl=fr&amp;q=J.B%20SPRINT%20SERVICES+(J.B%20SPRINT%20SERVICES)&amp;ie=UTF8&amp;t=&amp;z=10&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </div>
        <div class="col-lg-6">
          <div class="card" style="width: 100%;">
           <div class="card-body">
             <h5 class="card-title">Nous contacter</h5>
             <p class="card-text"><b>Adresse : </b>19 rue du Bois, 39410 Aumur, France</p>
             <p class="card-text"><b>Téléphone : </b>06 85 39 95 80</p>
             <p class="card-text"><b>Adresse e-mail : </b>info@jbsprintservices.fr</p>
             <p class="card-text"><b>Horaires : </b>Joignable 7j/7 et 24h/24</p>
             <a href="tel:0685399580" class="btn btn-success"><i class="fas fa-phone-alt"></i> Nous appeler au 06 85 39 95 80</a>
           </div>
         </div>
        </div>
     </div>
   </div>
</section>
@endsection
