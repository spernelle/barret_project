<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function admin($page)
    {
        $content=Content::where('slug',$page)->first();
        return view('admin.content')->with("content",$content);
    }

    public function save(Request $request,$page)
    {
        $content=Content::where('slug',$page)->first();
        $content->description= $request->input('description');
        $content->save();
        $request->session()->flash('success', 'Le contenu de la page '.$content->titre.' a été modifié');
        return redirect()->route("admin-index");
    }
}
