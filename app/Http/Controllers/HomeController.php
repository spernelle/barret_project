<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\Flotte;
use App\Photo;

class HomeController extends Controller
{
    public function accueil()
    {
        $accueil = Content::where('slug', 'accueil-livraison')->first();
        $transport = Content::where('slug', 'transport-livraison')->first();
        $flotte = Flotte::orderBy('nombre', 'ASC')->get();
        $photo = Photo::orderBy('id', 'DESC')->get();
        return view('home')->with('accueil', $accueil)->with('transport', $transport)->with('flotte', $flotte)->with('photo', $photo);
    }
}
