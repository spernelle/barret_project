<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(contents_seed::class);
        $this->call(photos_seed::class);
        $this->call(flottes_seed::class);
    }
}
