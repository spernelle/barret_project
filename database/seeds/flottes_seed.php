<?php

use Illuminate\Database\Seeder;

class flottes_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('flottes')->insert([
            'vehicule' => "Fourgon 3m3",
            'charge' => "600 kg",
            'nombre' => "2",
            'hauteur' => "1.10 m",
        ]);
        DB::table('flottes')->insert([
            'vehicule' => "Fourgon 6m3",
            'charge' => "1000 kg",
            'nombre' => "2",
            'hauteur' => "1.20 m",
        ]);
        DB::table('flottes')->insert([
            'vehicule' => "Fourgon 12m3",
            'charge' => "1500 kg",
            'nombre' => "3",
            'hauteur' => "1.75 m",
        ]);
        DB::table('flottes')->insert([
            'vehicule' => "Fourgon 14m3",
            'charge' => "1500 kg",
            'nombre' => "4",
            'hauteur' => "1.75 m",
        ]);    
        DB::table('flottes')->insert([
            'vehicule' => "Fourgon 20m3 avec hayon 750kg",
            'charge' => "850 kg",
            'nombre' => "8",
            'hauteur' => "2.10 m",
        ]);   
    }
}
