<?php

use Illuminate\Database\Seeder;

class contents_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->insert([
            'titre' => "Accueil livraison",
            'description' =>"Depuis 2005, l'entreprise <strong>JB Sprint Services</strong> intervient auprès des industriels, des professionnels et des administrations pour des <a><strong>transports urgents</strong>
                            </a> de marchandises et de <strong>matières dangereuses</strong> et explosives sur toutes distances en France et en Europe.<br/><br/>
                            Située entre <strong>Dijon</strong>, Beaune, <strong>Besançon</strong> et Lons-le-Saunier dans le Jura, une équipe de quatre chauffeurs professionnels formés et d'autant de véhicules peuvent prendre en charge vos colis jusqu'à 1,5 tonne, que ce soit pour des palettes d'outillage et de matériels, de la presse et des médicaments ou grâce à leur agrément ADR de transport de produits dangereux comme des produits chimiques, des peintures, des solvants et du gaz.<br/><br/>
                            Le transport se fait avec des véhicules parfaitement adaptés aux colis, et, dans le cadre de transport de matières dangereuses, un double équipage, la géolocalisation du véhicule pour un meilleur suivi, le marquage sur le toit pour un repérage aérien et une valise ADR qui contient tout l'équipement de sécurité nécessaire.<br/><br/>
                            Pour des renseignements complémentaires sur nos prestations, <a>contactez-nous</a>.<br/>",
            'slug' => "accueil-livraison",
            'photo' => "accueil.png",
        ]);
        DB::table('contents')->insert([
            'titre' => "Transport livraison",
            'description' =>"Avec son <strong>agr&eacute;ment ADR</strong> pour le transport de mati&egrave;res dangereuses et explosives comme des produits chimiques, des solvants ou du gaz, l&#39;entreprise est &agrave; m&ecirc;me de les livrer au niveau national et europ&eacute;en en transport r&eacute;gulier, sp&eacute;cial ou urgent.<br />
            <br />Le transport se fait avec des v&eacute;hicules de moins de 3,5 tonnes avec une capacit&eacute; de 1,5 tonne de marchandises. Les v&eacute;hicules sont g&eacute;olocalis&eacute;s, marqu&eacute;s sur le toit pour un rep&eacute;rage a&eacute;rien et des mesures de s&eacute;curit&eacute; sont mises en place : double &eacute;quipage form&eacute; au risque routier et valise ADR.<br />
            <br />Une assurance est mise en place &agrave; hauteur de 50 000&euro; pour vos marchandises en transports nationaux, de 75 000&euro; pour vos marchandises en transports internationaux et au-del&agrave; de ces seuils apr&egrave;s une &eacute;tude au cas par cas.	</div>
            ",
            'slug' => "transport-livraison",
            'photo' => "transport.png",
        ]);
    }
}
