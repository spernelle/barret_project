<?php

use Illuminate\Database\Seeder;

class photos_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('photos')->insert([
            'titre' => "Véhicule explosif",
            
            'photo' => "photo1.jpg",
        ]);
         DB::table('photos')->insert([
            'titre' => "Le parc",
            
            'photo' => "photo2.jpg",
        ]);
        DB::table('photos')->insert([
            'titre' => "Chargement matière dangereuse",
            
            'photo' => "photo3.jpg",
        ]);
        DB::table('photos')->insert([
            'titre' => "Fourgon 12m3",
            
            'photo' => "photo4.jpg",
        ]);
    }
}
